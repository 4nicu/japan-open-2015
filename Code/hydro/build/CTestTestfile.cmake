# CMake generated Testfile for 
# Source directory: /home/lattel/hydro/src
# Build directory: /home/lattel/hydro/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(gtest)
SUBDIRS(turtlebot_arm/turtlebot_arm)
SUBDIRS(turtlebot_arm/turtlebot_arm_bringup)
SUBDIRS(turtlebot_arm/turtlebot_arm_description)
SUBDIRS(turtlebot_arm/turtlebot_arm_moveit_demos)
SUBDIRS(basic_function)
SUBDIRS(kitchen)
SUBDIRS(turtlebot_arm/turtlebot_arm_kinect_calibration)
SUBDIRS(turtlebot_arm/turtlebot_arm_mp)
SUBDIRS(turtlebot_arm/turtlebot_arm_ikfast_plugin)
SUBDIRS(turtlebot_arm/turtlebot_arm_block_manipulation)
SUBDIRS(turtlebot_arm/turtlebot_arm_moveit_config)
