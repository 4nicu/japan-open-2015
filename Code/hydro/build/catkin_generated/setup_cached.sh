#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CATKIN_TEST_RESULTS_DIR="/home/lattel/hydro/build/test_results"
export CMAKE_PREFIX_PATH="/home/lattel/hydro/devel:$CMAKE_PREFIX_PATH"
export CPATH="/home/lattel/hydro/devel/include:$CPATH"
export LD_LIBRARY_PATH="/home/lattel/hydro/devel/lib:$LD_LIBRARY_PATH"
export PATH="/home/lattel/hydro/devel/bin:$PATH"
export PKG_CONFIG_PATH="/home/lattel/hydro/devel/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD="/home/lattel/hydro/build"
export PYTHONPATH="/home/lattel/hydro/devel/lib/python2.7/dist-packages:$PYTHONPATH"
export ROSLISP_PACKAGE_DIRECTORIES="/home/lattel/hydro/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/lattel/hydro/src:/home/lattel/turtlebot/src:/opt/ros/hydro/share:/opt/ros/hydro/stacks"
export ROS_TEST_RESULTS_DIR="/home/lattel/hydro/build/test_results"