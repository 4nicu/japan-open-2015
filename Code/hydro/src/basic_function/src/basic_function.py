#!/usr/bin/env python

"""
    basic_function.py - peforms tasks for basic functionality of RoboCup Japan Open.

"""

import roslib; roslib.load_manifest('pi_speech_tutorial')
import rospy
from std_msgs.msg import String, Header
from kobuki_msgs.msg import DigitalInputEvent

import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PoseWithCovariance
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionGoal
from tf.transformations import quaternion_from_euler

from sound_play.libsoundplay import SoundClient

point = 1
original = 0
place = 1
message = "none"
start = 0
num_order = 0
step = 0
stop = 0
count = 0

digital_in = [False, False, False, False]

class B_Function:
    def __init__(self):
        rospy.on_shutdown(self.cleanup)
          
        self.voice = rospy.get_param("~voice", "voice_don_diphone")
        self.wavepath = rospy.get_param("~wavepath", "")
        
        # Create the sound client object
        self.soundhandle = SoundClient()
        rospy.sleep(1)
        self.soundhandle.stopAll()

        # Announce that we are ready for input
        self.soundhandle.playWave(self.wavepath + "/R2D2a.wav")
        rospy.sleep(1)
	## IF GATE OPEN ##
        self.soundhandle.say("initiated", self.voice)
       	self.soundhandle.say("Please give command", self.voice)
	rospy.sleep(1)

        # Subscribe to the recognizer output
        rospy.Subscriber('/recognizer/output', String, self.identify)

    	# Subscribe to the /mobile_base/events/digital_input topic to receive digital input
    	rospy.Subscriber('/mobile_base/events/digital_input', DigitalInputEvent, self.DigitalInputEventCallback)

	# Publisher to manually control the robot
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

	# Subscribe to the move_base action server
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("Waiting for move_base action server...")
        # Wait for the action server to become available
        self.move_base.wait_for_server(rospy.Duration(120))
        rospy.loginfo("Connected to move base server")

        # A variable to hold the initial pose of the robot to be set by the user in RViz
        self.p = PoseWithCovarianceStamped()
	self.msg = PoseWithCovariance()
	self.init_pose_pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped)

	self.goal = MoveBaseGoal()
        rospy.loginfo("Starting navigation test")
	rospy.sleep(2)
	rospy.loginfo("1")

	# Not sure can work or not, need to test
	# Record initial coordinate manually (rostopic echo
	# /initialpose)
	# No need to use rviz every time
	self.msg.pose = Pose( Point(1.37290459773, 0.4268675966, 0.0), Quaternion(0.0, 0.0, -0.984356578764, 0.176187757361) )
	self.msg.covariance = [0.0030628982822256123, 0.000771303527867806, 0.0, 0.0, 0.0, 0.0, 0.000771303527867806, 0.002912862584606074, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0049039823453215]
	self.p.pose = self.msg
	self.p.header.frame_id = 'map'
	self.p.header.stamp = rospy.Time.now()

	rospy.loginfo("2")
	self.init_pose_pub.publish(self.p)
	rospy.loginfo("3")

	locations = dict()

	# coordinates where both objects are put.
	locations['A'] = Pose(Point(0.375943765473, 1.68450490393, 0.0), Quaternion(0.0, 0.0, 0.0187100099758, 0.999824952443))

	# coordinate where to put the known object
	locations['B'] = Pose(Point(-2.13582652729, 0.996369393954, 0.0), Quaternion(0.0, 0.0, -0.970761641226, 0.2400454872))

	# coordinate where to throw the unknown object
	locations['C'] = Pose(Point(-0.768971946954, 2.53877019797, 0.0), Quaternion(0.0, 0.0, 0.804571558261, 0.593855712811))

	# retreat to a safer area
	locations['D'] = Pose(Point(-4.2786050667, -3.02841219828, 0.0), Quaternion(0.0, 0.0, -0.5400172439, 0.841653952816))

	# coordinate where the task "What did you say" is carry out
	locations['E'] = Pose(Point(-5.70868224003, -0.214638157354, 0.0), Quaternion(0.0, 0.0, 0.997961524127, 0.0638184641142))


	global start
	global step

	while not rospy.is_shutdown():
	  self.goal.target_pose.header.frame_id = 'map'
	  self.goal.target_pose.header.stamp = rospy.Time.now()

	  if stop == 1:
            self.cmd_vel_pub.publish(Twist())
	  elif stop == 0:
	    if start == 1:
		self.soundhandle.say("going to first destination", self.voice)
	  	self.goal.target_pose.pose = locations['A']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
                    self.soundhandle.say("Reached table", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Identifying known object", self.voice)
                    rospy.sleep(5)
		    # object recognising #			
                    self.soundhandle.say("Object found", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Grabing object", self.voice)
                    rospy.sleep(6)
		    # arm movement #
                    self.soundhandle.say("Ready to put object on another table", self.voice)
                    rospy.sleep(6)
                    start = 2

	    elif start == 2:
	  	self.goal.target_pose.pose = locations['B']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
                    self.soundhandle.say("Putting object on the table", self.voice)
                    rospy.sleep(6)
		    # arm movement #
                    self.soundhandle.say("Go back to table and take the unknown object", self.voice)
                    rospy.sleep(5)
                    start = 5
		    step = 1

	    elif start == 3:
	  	self.goal.target_pose.pose = locations['A']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
                    self.soundhandle.say("Reached table", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Identifying unknown object", self.voice)
                    rospy.sleep(5)	
		    # object recognising #		
                    self.soundhandle.say("Object found", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Grabing object", self.voice)
                    rospy.sleep(6)
		    # arm movement #
		    self.soundhandle.say("Ready to throw the object", self.voice)
                    rospy.sleep(6)
                    start = 4

	    elif start == 4:
	  	self.goal.target_pose.pose = locations['C']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
                    self.soundhandle.say("Reached dustbin", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Throwing object", self.voice)
                    rospy.sleep(5)
		    # arm movement #	
                    self.soundhandle.say("Going to safe zone", self.voice)
                    rospy.sleep(4)
                    start = 5

	    elif start == 5:
		# move to decoy position (left)
		self.goal.target_pose.pose = Pose(Point(-2.17235849614, -0.258467709475, 0.0), Quaternion(0.0, 0.0, -0.958025360182, 0.28668346525))
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))

		# move to center of room
		self.goal.target_pose.pose = Pose(Point(-0.937896928669, 1.03749765754, 0.0), Quaternion(0.0, 0.0, -0.0222151207941, 0.999753213752))
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))

		# move to table right
		self.goal.target_pose.pose = Pose(Point(-2.68258180383, 2.02915501828, 0.0), Quaternion(0.0, 0.0, -0.99823110197, 0.05945306602))
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		#further right
		self.goal.target_pose.pose = Pose(Point(-3.80682846512, 1.60609658054, 0.0), Quaternion(0.0, 0.0, -0.747338260884, 0.664443770246))
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))

		# move to exit point
  		self.goal.target_pose.pose = locations['E']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
                    self.soundhandle.say("Reached safe zone", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Waiting for command", self.voice)
                    rospy.sleep(4)

		    global step
		    step = 1
                    start = 6

	    elif start == 6:
		# go to talkback location
	  	self.goal.target_pose.pose = locations['E']
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(600))
		if waiting == 1:
                    self.soundhandle.say("Reached next location", self.voice)
                    rospy.sleep(2)
                    self.soundhandle.say("Locating people", self.voice)
                    rospy.sleep(2)
		    self.soundhandle.say("Target found", self.voice)			
		    # will further develop if PCL implemented
                    start = 100

            rospy.Rate(3).sleep()

    def update_initial_pose(self, initial_pose):
        self.initial_pose = initial_pose
	global original
	if original == 0:
            self.origin = self.initial_pose.pose.pose
            #global original
            original = 1

    def identify(self, msg):
	rospy.loginfo(msg.data)
	global step
	global locations
	global start
	global count

	if step == 0:
	    if msg.data == 'turtlebot, start your journey' or msg.data == 'journey' or msg.data == 'start your journey':
		rospy.sleep(1)
		self.soundhandle.say("Ready to go", self.voice)
		rospy.sleep(2)

		#step = 100
		## CHANGE TESTING MODE HERE ##
		## SET DEFAULT TO 1 ##
		start = 1
		#start = 100
		

	elif step == 1 and start == 100:
	    if msg.data == 'turtlebot, proceed to the next task' or msg.data == 'proceed' or msg.data == 'proceed to the next task':
		rospy.loginfo(msg.data)
		rospy.sleep(1)
		self.soundhandle.say("Understood", self.voice)
		rospy.sleep(2)

		step = 100
		#start = 6

	elif step == 100 and start == 100:
	  if msg.data == 'turtlebot,' or msg.data == 'journey':
		step = 200
	  if count < 3:
	# Question 1
	    if msg.data == 'your name' or msg.data == 'what is your name':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is my name?", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("My name is turtle bot", self.voice)
		rospy.sleep(3)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 2
	    if (msg.data == 'what day is today' and not msg.data == 'weather')or (msg.data == 'today' and not msg.data == 'weather'):
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What day is today?", self.voice)
		rospy.sleep(4)
		# please change the day !!!!!!
		self.soundhandle.say("Today is Friday", self.voice)
		rospy.sleep(3)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 3
	    if msg.data == 'what is today weather' or msg.data == 'weather':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is today's weather?", self.voice)
		rospy.sleep(4)
		# please change the weather !!!!!!
		self.soundhandle.say("Today is very cloudy", self.voice)
		rospy.sleep(3)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 4
	    if msg.data == 'what is the capital of Japan' or msg.data == 'japan':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is the capital of Japan?", self.voice)
		rospy.sleep(5)
		self.soundhandle.say("Tokyo is Japan's capital", self.voice)
		rospy.sleep(3)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 5
	    if msg.data == 'what is the' or msg.data == 'mountain' or msg.data == 'fuji':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is the height of mountain fuji?", self.voice)
		rospy.sleep(5)
		self.soundhandle.say("three thousands seven hundreds and seventy six meters", self.voice)
		rospy.sleep(6)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 6
	    if msg.data == 'what is the longest river' or msg.data == 'longest' or msg.data == 'river':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is the longest river in the world?", self.voice)
		rospy.sleep(5)
		self.soundhandle.say("it is Nile River in Egypt", self.voice)
		rospy.sleep(5)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 7
	    if msg.data == 'who is the american president ' or msg.data == 'american' or msg.data == 'president':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("Who is an American President?", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("President Barack Obama", self.voice)
		rospy.sleep(4)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 8
	    if msg.data == 'which is' or msg.data == 'japan' or msg.data == 'china':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("Which is bigger? Japan or China?", self.voice)
		rospy.sleep(6)
		self.soundhandle.say("China is bigger than Japan", self.voice)
		rospy.sleep(5)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 9
	    if msg.data == 'what is the heaviest' or msg.data == 'heaviest' or msg.data == 'animal':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("What is the heaviest animal in the world?", self.voice)
		rospy.sleep(6)
		self.soundhandle.say("Blue whale is the heaviest animal on earth", self.voice)
		rospy.sleep(5)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	# Question 10
	    if msg.data == 'how many' or msg.data == 'cow':
		self.soundhandle.say("I will repeat the question again", self.voice)
		rospy.sleep(4)
		self.soundhandle.say("How many legs does the cow have?", self.voice)
		rospy.sleep(5)
		self.soundhandle.say("Cows have four legs", self.voice)
		rospy.sleep(5)
		count+=1
		if count < 3:
		    self.soundhandle.say("Next question please", self.voice)
		    rospy.sleep(2)
	  	if count >= 3:
	    	    self.soundhandle.say("I have finished all three questions", self.voice)
	    	    rospy.sleep(4)
	    	    step = 200

	if step == 200:
	    self.soundhandle.say("Goodbye", self.voice)
            rospy.sleep(1)
		## MOVING TO EXIT POINT ##
	    self.goal.target_pose.pose = Pose(Point(-4.2786050667, -3.02841219828, 0.0), Quaternion(0.0, 0.0, -0.5400172439, 0.841653952816))
	    self.move_base.send_goal(self.goal)
	    waiting = self.move_base.wait_for_result(rospy.Duration(300))
		## END ##

    def DigitalInputEventCallback(self, data):
    	global digital_in
	global stop
    	digital_in = data.values
	if digital_in[3] == True:	# emergency button pressed
	    stop = 1
	    rospy.loginfo("Button RED pressed !!!")
	elif digital_in[3] == False:	# emergency button not pressed
	    stop = 0
	    rospy.loginfo("Safe")

    def cleanup(self):
        rospy.loginfo("Shutting down navigation	....")
	self.move_base.cancel_goal()
        self.cmd_vel_pub.publish(Twist())

if __name__=="__main__":
    rospy.init_node('basic_function')
    try:
        B_Function()
        #rospy.spin()
    except:
        pass

